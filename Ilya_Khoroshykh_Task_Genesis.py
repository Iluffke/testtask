#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  9 19:56:07 2017

@author: ilya
"""

import pandas as pd
from datetime import datetime
from dateutil import relativedelta as rd

#ROI by period counting method
def roiByPeriod(dataset,dateField,delta=0,startIndex=0):
    s1=0
    s2=0
    roicounter=(lambda x,y: ((x-y)/y))
    roiByPeriod=0
    endIndex=0
    if delta==0:
        endIndex=startIndex
    else:
        endDate=dataset[dateField][startIndex]+delta
        endIndex=pd.Index(dataset[dateField]).get_loc(endDate)
        #print (endIndex)
    for x in (startIndex,endIndex):
        amount=dataset['amount'][x]
        cost=dataset['cost'][x]
        if (cost is 0):
            roiByPeriod=(roicounter(s1+amount,s2))
        else:
            roiByPeriod+=(roicounter(s1+amount,s2+cost))
        s1+=dataset['amount'][x]
        s2+=dataset['cost'][x]
    return roiByPeriod
  
#Importing starting datasets
file=pd.ExcelFile('Raw data, test.xlsx')
costsBySource=file.parse('Costs by source')
users=file.parse('Users')
orders=file.parse('Orders')

#unifying the date formate
costsBySource['date_formated']=[x.date() for x in costsBySource['date']]
del costsBySource['date']

orders['date_order_formated']=[x.date() for x in orders['date_order']]
del orders['date_order']

users['date_created_formated']=[x.date() for x in users['date_created']]
del users['date_created']

#joining our datasets for ROI and CPR counting
users.columns=['id_user','source','date_created_formated']
ordersToSources=orders.merge(users,'inner', on=['id_user'])

#creating grouped datasets for ROI counting
ordersToSourcesGrouped=ordersToSources.drop(['id_user','date_created_formated'],axis='columns').groupby(['source','date_order_formated']).sum().reset_index().drop(['id'],axis='columns')

#renaiming columns for merging
ordersToSourcesGrouped.columns=['source','date_formated','amount']

balance=ordersToSourcesGrouped.merge(costsBySource,'left',on=['source','date_formated']).fillna(0)
#splitting our datetime objects by time dimensions for more comfortable work and more understendable code
balance['year']=[x.year for x in balance.date_formated]
#balance['after_halfyear']=[x.month%12 for x in balance.date_formated]
#balance[balance['after_halfyear']==0]=12
balance['month']=[x.month for x in balance.date_formated]
balance['week']=[int(x/7)+1 for x in balance.date_formated.index]
balance['day']=[x.day for x in balance.date_formated]

#grouping our datasets by needable time dimensions (weeks,monthes et cetera)
weekGrouped=balance.groupby(['source','year','month','week']).sum().drop(['day'],axis='columns').reset_index()
monthGrouped=balance.groupby(['source','year','month']).sum().drop(['day','week'],axis='columns').reset_index()
dates=[]
for x in monthGrouped['source'].index:
    dates.append(datetime(year=monthGrouped.year[x],month=monthGrouped.month[x],day=1).date())
monthGrouped['date_formated']=dates

#ROI by period counting
roi0day=[roiByPeriod(balance,balance.columns[1],delta=0,startIndex=x) for x in [balance[balance['day']==1].index]][0]
roi1week=[roiByPeriod(weekGrouped,weekGrouped.columns[1],delta=0,startIndex=x) for x in weekGrouped['week'].index]
roi1month=[roiByPeriod(monthGrouped,monthGrouped.columns[1],delta=0,startIndex=x) for x in monthGrouped['month'].index]
    
#adding our ROI lists into our datasets
balance['roi_0_day']=roi0day
weekGrouped['roi_1_week']=roi1week
monthGrouped['roi_1_month']=roi1month

#because length of our dataset is 8 months, we could count halfyear ROI only for 9 cases (3 sources for 3 months). So, we could do it in kind of naive way :) :
halfyear=rd.relativedelta(months=6)
roi6month={1:'',
           2:'',
           3:''}
for y in [1,2,3]:
    workingSet=monthGrouped[monthGrouped['source']==y].reset_index().drop(['index'],axis='columns')
    roi6month[y]=[roiByPeriod(workingSet,workingSet.columns[5],delta=halfyear,startIndex=x) for x in [0,1,2]]