#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 12 15:18:34 2017

@author: ilya
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  9 19:56:07 2017

@author: ilya
"""

import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
#from dateutil import relativedelta as rd
  
#Importing starting datasets
file=pd.ExcelFile('Raw data, test.xlsx')
costsBySource=file.parse('Costs by source')
users=file.parse('Users')
orders=file.parse('Orders')

#unifying the date formate
costsBySource['date_formated']=[x.date() for x in costsBySource['date']]
del costsBySource['date']

orders['date_formated']=[x.date() for x in orders['date_order']]
del orders['date_order']

users['date_formated']=[x.date() for x in users['date_created']]
del users['date_created']

usersBySourceCount=users.groupby(['source','date_formated']).count().reset_index()
costsByRegistration=costsBySource.merge(usersBySourceCount,'inner',on=['source','date_formated'])
#costsByRegistration['year']=[x.year for x in costsByRegistration['date_formated']]
#costsByRegistration['month']=[x.month for x in costsByRegistration['date_formated']]
#costsByRegistration=costsByRegistration.groupby(['source','year','month']).sum().reset_index()
costsByRegistration['cpr']=[costsByRegistration['cost'][x]/costsByRegistration['id'][x] for x in costsByRegistration['id'].index]
for x in (1,2,3):
    workingSet=costsByRegistration[costsByRegistration['source']==x]
    cpr=pd.Series(data=workingSet['cpr'].tolist(),index=workingSet['date_formated'].tolist())
    plt.figure()
    print('Source #'+str(x))
    cpr.plot(label='Source #'+str(x))